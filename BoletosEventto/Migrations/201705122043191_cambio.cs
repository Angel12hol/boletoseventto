namespace BoletosEventto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cambio : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Facturas", "Usuario_UsuarioId", "dbo.Usuarios");
            DropIndex("dbo.Facturas", new[] { "Usuario_UsuarioId" });
            DropColumn("dbo.Facturas", "Usuario_UsuarioId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Facturas", "Usuario_UsuarioId", c => c.Int());
            CreateIndex("dbo.Facturas", "Usuario_UsuarioId");
            AddForeignKey("dbo.Facturas", "Usuario_UsuarioId", "dbo.Usuarios", "UsuarioId");
        }
    }
}
