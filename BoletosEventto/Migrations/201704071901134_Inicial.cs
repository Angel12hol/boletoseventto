namespace BoletosEventto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Boletoes",
                c => new
                    {
                        BoletoId = c.Int(nullable: false, identity: true),
                        Precio = c.Double(nullable: false),
                        Cantidad = c.Int(nullable: false),
                        EventoId = c.Int(nullable: false),
                        TipoBoletoId = c.Int(nullable: false),
                        PromocionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BoletoId)
                .ForeignKey("dbo.Eventoes", t => t.EventoId, cascadeDelete: true)
                .ForeignKey("dbo.Promocions", t => t.PromocionId, cascadeDelete: true)
                .ForeignKey("dbo.TipoBoletoes", t => t.TipoBoletoId, cascadeDelete: true)
                .Index(t => t.EventoId)
                .Index(t => t.TipoBoletoId)
                .Index(t => t.PromocionId);
            
            CreateTable(
                "dbo.Eventoes",
                c => new
                    {
                        EventoId = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false, maxLength: 50),
                        Fecha = c.String(nullable: false, maxLength: 20),
                        Localidad = c.String(nullable: false, maxLength: 30),
                        CategoriaId = c.Int(nullable: false),
                        Entradas = c.Int(nullable: false),
                        Columna = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EventoId)
                .ForeignKey("dbo.Categorias", t => t.CategoriaId, cascadeDelete: true)
                .Index(t => t.CategoriaId);
            
            CreateTable(
                "dbo.Categorias",
                c => new
                    {
                        CategoriaId = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.CategoriaId);
            
            CreateTable(
                "dbo.Promocions",
                c => new
                    {
                        PromocionId = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false, maxLength: 30),
                        Descuento = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.PromocionId);
            
            CreateTable(
                "dbo.TipoBoletoes",
                c => new
                    {
                        TipoBoletoId = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false, maxLength: 15),
                    })
                .PrimaryKey(t => t.TipoBoletoId);
            
            CreateTable(
                "dbo.DetalleFacturas",
                c => new
                    {
                        DetalleFacturaId = c.Int(nullable: false, identity: true),
                        BoletoId = c.Int(nullable: false),
                        FacturaId = c.Int(nullable: false),
                        Asiento = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DetalleFacturaId)
                .ForeignKey("dbo.Boletoes", t => t.BoletoId, cascadeDelete: true)
                .ForeignKey("dbo.Facturas", t => t.FacturaId, cascadeDelete: true)
                .Index(t => t.BoletoId)
                .Index(t => t.FacturaId);
            
            CreateTable(
                "dbo.Facturas",
                c => new
                    {
                        FacturaId = c.Int(nullable: false, identity: true),
                        UsuarioId = c.Int(nullable: false),
                        Total = c.Double(nullable: false),
                        Transaccion = c.String(nullable: false, maxLength: 25),
                    })
                .PrimaryKey(t => t.FacturaId)
                .ForeignKey("dbo.Usuarios", t => t.UsuarioId, cascadeDelete: true)
                .Index(t => t.UsuarioId);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        UsuarioId = c.Int(nullable: false, identity: true),
                        Nick = c.String(nullable: false, maxLength: 50),
                        Contrasena = c.String(nullable: false, maxLength: 50),
                        RolId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UsuarioId)
                .ForeignKey("dbo.Rols", t => t.RolId, cascadeDelete: true)
                .Index(t => t.RolId);
            
            CreateTable(
                "dbo.Rols",
                c => new
                    {
                        RolId = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.RolId);
            
            CreateTable(
                "dbo.DetalleStaffs",
                c => new
                    {
                        DetalleStaffId = c.Int(nullable: false, identity: true),
                        StaffId = c.Int(nullable: false),
                        EventoId = c.Int(nullable: false),
                        Cantidad = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DetalleStaffId)
                .ForeignKey("dbo.Eventoes", t => t.EventoId, cascadeDelete: true)
                .ForeignKey("dbo.Staffs", t => t.StaffId, cascadeDelete: true)
                .Index(t => t.StaffId)
                .Index(t => t.EventoId);
            
            CreateTable(
                "dbo.Staffs",
                c => new
                    {
                        StaffId = c.Int(nullable: false, identity: true),
                        Trabajador = c.String(nullable: false),
                        Costo = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.StaffId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DetalleStaffs", "StaffId", "dbo.Staffs");
            DropForeignKey("dbo.DetalleStaffs", "EventoId", "dbo.Eventoes");
            DropForeignKey("dbo.DetalleFacturas", "FacturaId", "dbo.Facturas");
            DropForeignKey("dbo.Facturas", "UsuarioId", "dbo.Usuarios");
            DropForeignKey("dbo.Usuarios", "RolId", "dbo.Rols");
            DropForeignKey("dbo.DetalleFacturas", "BoletoId", "dbo.Boletoes");
            DropForeignKey("dbo.Boletoes", "TipoBoletoId", "dbo.TipoBoletoes");
            DropForeignKey("dbo.Boletoes", "PromocionId", "dbo.Promocions");
            DropForeignKey("dbo.Boletoes", "EventoId", "dbo.Eventoes");
            DropForeignKey("dbo.Eventoes", "CategoriaId", "dbo.Categorias");
            DropIndex("dbo.DetalleStaffs", new[] { "EventoId" });
            DropIndex("dbo.DetalleStaffs", new[] { "StaffId" });
            DropIndex("dbo.Usuarios", new[] { "RolId" });
            DropIndex("dbo.Facturas", new[] { "UsuarioId" });
            DropIndex("dbo.DetalleFacturas", new[] { "FacturaId" });
            DropIndex("dbo.DetalleFacturas", new[] { "BoletoId" });
            DropIndex("dbo.Eventoes", new[] { "CategoriaId" });
            DropIndex("dbo.Boletoes", new[] { "PromocionId" });
            DropIndex("dbo.Boletoes", new[] { "TipoBoletoId" });
            DropIndex("dbo.Boletoes", new[] { "EventoId" });
            DropTable("dbo.Staffs");
            DropTable("dbo.DetalleStaffs");
            DropTable("dbo.Rols");
            DropTable("dbo.Usuarios");
            DropTable("dbo.Facturas");
            DropTable("dbo.DetalleFacturas");
            DropTable("dbo.TipoBoletoes");
            DropTable("dbo.Promocions");
            DropTable("dbo.Categorias");
            DropTable("dbo.Eventoes");
            DropTable("dbo.Boletoes");
        }
    }
}
