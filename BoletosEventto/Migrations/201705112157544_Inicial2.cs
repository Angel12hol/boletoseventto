namespace BoletosEventto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicial2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Facturas", "UsuarioId", "dbo.Usuarios");
            DropIndex("dbo.Facturas", new[] { "UsuarioId" });
            RenameColumn(table: "dbo.Facturas", name: "UsuarioId", newName: "Usuario_UsuarioId");
            AddColumn("dbo.Eventoes", "UsuarioId", c => c.Int(nullable: false));
            AddColumn("dbo.Facturas", "Cliente", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Facturas", "Usuario_UsuarioId", c => c.Int());
            CreateIndex("dbo.Eventoes", "UsuarioId");
            CreateIndex("dbo.Facturas", "Usuario_UsuarioId");
            AddForeignKey("dbo.Eventoes", "UsuarioId", "dbo.Usuarios", "UsuarioId", cascadeDelete: true);
            AddForeignKey("dbo.Facturas", "Usuario_UsuarioId", "dbo.Usuarios", "UsuarioId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Facturas", "Usuario_UsuarioId", "dbo.Usuarios");
            DropForeignKey("dbo.Eventoes", "UsuarioId", "dbo.Usuarios");
            DropIndex("dbo.Facturas", new[] { "Usuario_UsuarioId" });
            DropIndex("dbo.Eventoes", new[] { "UsuarioId" });
            AlterColumn("dbo.Facturas", "Usuario_UsuarioId", c => c.Int(nullable: false));
            DropColumn("dbo.Facturas", "Cliente");
            DropColumn("dbo.Eventoes", "UsuarioId");
            RenameColumn(table: "dbo.Facturas", name: "Usuario_UsuarioId", newName: "UsuarioId");
            CreateIndex("dbo.Facturas", "UsuarioId");
            AddForeignKey("dbo.Facturas", "UsuarioId", "dbo.Usuarios", "UsuarioId", cascadeDelete: true);
        }
    }
}
