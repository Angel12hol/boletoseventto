﻿using BoletosEventto.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace BoletosEventto.Controllers
{
    public class UsuarioController : ApiController
    {
        private BoletosEventtoContext db = new BoletosEventtoContext();
        //Get api/Usuario
        public IEnumerable<Usuario> GetUsuarios()
        {
            return db.Usuario.Include(c => c.Rol);
        }

        //GET: api/Usuario/id
        [ResponseType(typeof(Usuario))]
        public async Task<IHttpActionResult> GetUsuario(int id)
        {
            Usuario prom = await db.Usuario.Include(c => c.Rol).FirstOrDefaultAsync(c => c.UsuarioId == id); ;

            if (prom == null)
            {
                return NotFound();
            }

            return Ok(prom);
        }
        [HttpGet]
        [Route("api/Usuario/login/{nick}/{contrasena}")]
        [ResponseType(typeof(Usuario))]
        public async Task<IHttpActionResult> Login(String nick, String contrasena)
        {
            Usuario prom = await db.Usuario.Where(u => u.Nick.Equals(nick) && u.Contrasena.Equals(contrasena)).FirstOrDefaultAsync();

            if (prom == null)
            {
                return Json(new { result = "false" });
            }
            return Ok(prom);
        }

        //PUT: api/Usuario
        public async Task<IHttpActionResult> PutUsuario(int id, Usuario dato)
        {
            if (!ModelState.IsValid || id != dato.UsuarioId)
            {
                return BadRequest(ModelState);
            }
            db.Entry(dato).State = EntityState.Modified;
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return CreatedAtRoute("DefaultApi", new { id = dato.UsuarioId }, dato);
        }

        //DELETE: api/Usuario/id
        public async Task<IHttpActionResult> DeleteUsuario(int id)
        {
            Usuario dato = await db.Usuario.FindAsync(id);
            if (dato == null)
            {
                return NotFound();
            }
            db.Usuario.Remove(dato);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi", new { id = dato.UsuarioId }, dato);
        }

        [ResponseType(typeof(Usuario))]
        public async Task<IHttpActionResult> PostUsuario(Usuario dato)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Usuario.Add(dato);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi", new { id = dato.UsuarioId }, dato);
        }
    }
}
