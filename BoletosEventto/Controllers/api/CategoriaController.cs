﻿using BoletosEventto.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace BoletosEventto.Controllers
{
    public class CategoriaController : ApiController
    {
        private BoletosEventtoContext db = new BoletosEventtoContext();
        //Get api/Categoria
        public IEnumerable<Categoria> GetCategorias()
        {
            return db.Categoria;
        }

        //GET: api/Categoria/id
        [ResponseType(typeof(Categoria))]
        public async Task<IHttpActionResult> GetContacto(int id)
        {
            Categoria cat = await db.Categoria.FindAsync(id);

            if (cat == null)
            {
                return NotFound();
            }

            return Ok(cat);
        }

        //PUT: api/Categoria
        public async Task<IHttpActionResult> PutCategoria(int id, Categoria categoria)
        {
            if (!ModelState.IsValid || id != categoria.CategoriaId)
            {
                return BadRequest(ModelState);
            }
            db.Entry(categoria).State = EntityState.Modified;
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return CreatedAtRoute("DefaultApi", new { id = categoria.CategoriaId }, categoria);
        }

        //DELETE: api/Categoria/id
        public async Task<IHttpActionResult> DeleteCategoria(int id)
        {
            Categoria categoria = await db.Categoria.FindAsync(id);
            if (categoria == null)
            {
                return NotFound();
            }
            db.Categoria.Remove(categoria);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi", new { id = categoria.CategoriaId }, categoria);
        }

        [ResponseType(typeof(Categoria))]
        public async Task<IHttpActionResult> PostCategoria(Categoria categoria)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Categoria.Add(categoria);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi", new { id = categoria.CategoriaId }, categoria);
        }
    }
}
