﻿using BoletosEventto.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace BoletosEventto.Controllers
{
    public class TipoBoletoController : ApiController
    {
        private BoletosEventtoContext db = new BoletosEventtoContext();
        //Get api/TipoBoleto
        public IEnumerable<TipoBoleto> GetTiposBoletos()
        {
            return db.TipoBoleto;
        }

        //GET: api/TipoBoleto/id
        [ResponseType(typeof(TipoBoleto))]
        public async Task<IHttpActionResult> GetTipoBoleto(int id)
        {
            TipoBoleto prom = await db.TipoBoleto.FindAsync(id);

            if (prom == null)
            {
                return NotFound();
            }

            return Ok(prom);
        }

        //PUT: api/TipoBoleto
        public async Task<IHttpActionResult> PutTipoBoleto(int id, TipoBoleto dato)
        {
            if (!ModelState.IsValid || id != dato.TipoBoletoId)
            {
                return BadRequest(ModelState);
            }
            db.Entry(dato).State = EntityState.Modified;
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return CreatedAtRoute("DefaultApi", new { id = dato.TipoBoletoId }, dato);
        }

        //DELETE: api/TipoBoleto/id
        public async Task<IHttpActionResult> DeleteTipoBoleto(int id)
        {
            TipoBoleto dato = await db.TipoBoleto.FindAsync(id);
            if (dato == null)
            {
                return NotFound();
            }
            db.TipoBoleto.Remove(dato);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi", new { id = dato.TipoBoletoId }, dato);
        }

        [ResponseType(typeof(TipoBoleto))]
        public async Task<IHttpActionResult> PostTipoBoleto(TipoBoleto dato)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.TipoBoleto.Add(dato);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi", new { id = dato.TipoBoletoId }, dato);
        }
    }
}
