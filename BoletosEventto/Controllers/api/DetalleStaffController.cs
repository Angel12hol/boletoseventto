﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BoletosEventto.Models;
using System.Threading.Tasks;

namespace BoletosEventto.Controllers.api
{
    public class DetalleStaffController : ApiController
    {
        private BoletosEventtoContext db = new BoletosEventtoContext();

        // GET: api/DetalleStaff
        public IQueryable<DetalleStaff> GetDetalleStaff()
        {
            return db.DetalleStaff.Include(c => c.Staff).Include(c => c.Evento);
        }

        // GET: api/DetalleStaff/5
        [ResponseType(typeof(DetalleStaff))]
        public async Task<IHttpActionResult> GetDetalleStaff(int id)
        {
            DetalleStaff detalleStaff = await db.DetalleStaff.Include(c => c.Staff).Include(c => c.Evento).FirstOrDefaultAsync(c => c.DetalleStaffId == id);
            if (detalleStaff == null)
            {
                return NotFound();
            }

            return Ok(detalleStaff);
        }


        // PUT: api/DetalleStaff/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDetalleStaff(int id, DetalleStaff detalleStaff)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != detalleStaff.DetalleStaffId)
            {
                return BadRequest();
            }

            db.Entry(detalleStaff).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DetalleStaffExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DetalleStaff
        [ResponseType(typeof(DetalleStaff))]
        public IHttpActionResult PostDetalleStaff(DetalleStaff detalleStaff)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DetalleStaff.Add(detalleStaff);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = detalleStaff.DetalleStaffId }, detalleStaff);
        }

        // DELETE: api/DetalleStaff/5
        [ResponseType(typeof(DetalleStaff))]
        public IHttpActionResult DeleteDetalleStaff(int id)
        {
            DetalleStaff detalleStaff = db.DetalleStaff.Find(id);
            if (detalleStaff == null)
            {
                return NotFound();
            }

            db.DetalleStaff.Remove(detalleStaff);
            db.SaveChanges();

            return Ok(detalleStaff);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DetalleStaffExists(int id)
        {
            return db.DetalleStaff.Count(e => e.DetalleStaffId == id) > 0;
        }
    }
}