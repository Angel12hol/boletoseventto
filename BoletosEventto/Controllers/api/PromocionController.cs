﻿using BoletosEventto.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace BoletosEventto.Controllers
{
    public class PromocionController : ApiController
    {
        private BoletosEventtoContext db = new BoletosEventtoContext();
        //Get api/Promocion
        public IEnumerable<Promocion> GetPromociones()
        {
            return db.Promocion;
        }

        //GET: api/Promocion/id
        [ResponseType(typeof(Promocion))]
        public async Task<IHttpActionResult> GetPromocion(int id)
        {
            Promocion prom = await db.Promocion.FindAsync(id);

            if (prom == null)
            {
                return NotFound();
            }

            return Ok(prom);
        }

        //PUT: api/Promocion
        public async Task<IHttpActionResult> PutPromocion(int id, Promocion dato)
        {
            if (!ModelState.IsValid || id != dato.PromocionId)
            {
                return BadRequest(ModelState);
            }
            db.Entry(dato).State = EntityState.Modified;
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return CreatedAtRoute("DefaultApi", new { id = dato.PromocionId }, dato);
        }

        //DELETE: api/Promocion/id
        public async Task<IHttpActionResult> DeletePromocion(int id)
        {
            Promocion dato = await db.Promocion.FindAsync(id);
            if (dato == null)
            {
                return NotFound();
            }
            db.Promocion.Remove(dato);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi", new { id = dato.PromocionId }, dato);
        }

        [ResponseType(typeof(Categoria))]
        public async Task<IHttpActionResult> PostPromocion(Promocion dato)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Promocion.Add(dato);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi", new { id = dato.PromocionId }, dato);
        }
    }
}
