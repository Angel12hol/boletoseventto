﻿using BoletosEventto.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace BoletosEventto.Controllers
{
    public class RolController : ApiController
    {
        private BoletosEventtoContext db = new BoletosEventtoContext();
        //Get api/Rol
        public IEnumerable<Rol> GetRoles()
        {
            return db.Rol;
        }

        //GET: api/Rol/id
        [ResponseType(typeof(Rol))]
        public async Task<IHttpActionResult> GetRol(int id)
        { 
            Rol prom = await db.Rol.FindAsync(id);

            if (prom == null)
            {
                return NotFound();
            }

            return Ok(prom);
        }

        //PUT: api/Rol
        public async Task<IHttpActionResult> PutRol(int id, Rol dato)
        {
            if (!ModelState.IsValid || id != dato.RolId)
            {
                return BadRequest(ModelState);
            }
            db.Entry(dato).State = EntityState.Modified;
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return CreatedAtRoute("DefaultApi", new { id = dato.RolId }, dato);
        }

        //DELETE: api/Rol/id
        public async Task<IHttpActionResult> DeleteRol(int id)
        {
            Rol dato = await db.Rol.FindAsync(id);
            if (dato == null)
            {
                return NotFound();
            }
            db.Rol.Remove(dato);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi", new { id = dato.RolId }, dato);
        }

        [ResponseType(typeof(Rol))]
        public async Task<IHttpActionResult> PostRol(Rol dato)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Rol.Add(dato);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi", new { id = dato.RolId }, dato);
        }
    }
}
