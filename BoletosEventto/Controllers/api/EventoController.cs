﻿using BoletosEventto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;


namespace BoletosEventto.Controllers
{
    public class EventoController : ApiController
    {
		private BoletosEventtoContext db = new BoletosEventtoContext();

		public IQueryable<Evento> GetEventos()
		{
			return db.Evento.Include(c => c.Categoria).OrderBy(c => c.Fecha);

		}

		[ResponseType(typeof(Evento))]
		public async Task<IHttpActionResult> GetEvento(int id)
		{
			Evento evento = await db.Evento.Include(c => c.Categoria).FirstOrDefaultAsync(c => c.EventoId == id);
			if (evento == null)
			{
				return NotFound();
			}
			return Ok(evento);
		}

		[ResponseType(typeof(Evento))]
		public async Task<IHttpActionResult> PostEvento(Evento evento)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			db.Evento.Add(evento);
			await db.SaveChangesAsync();
			return CreatedAtRoute("DefaultApi", 
				new { id = evento.EventoId }, evento);
		}

		[ResponseType(typeof(Evento))]
		public async Task<IHttpActionResult> PutEvento(int id, Evento evento)
		{
			if (!ModelState.IsValid || id != evento.EventoId)
			{
				return BadRequest(ModelState);
			}

			db.Entry(evento).State = EntityState.Modified;
			try
			{
				await db.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				throw;
			}

			return CreatedAtRoute("DefaultApi",
				new { id = evento.EventoId }, evento);

		}
		[ResponseType(typeof(Evento))]
		public async Task<IHttpActionResult> DeleteEvento(int id)
		{
			Evento evento = await db.Evento.FindAsync(id);
			if (evento == null)
			{
				return NotFound();
			}
			db.Evento.Remove(evento);
			await db.SaveChangesAsync();

			return CreatedAtRoute("DefaultApi", new { id = evento.EventoId }, evento);
		}



	}

}
