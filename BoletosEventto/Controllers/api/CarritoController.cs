﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using BoletosEventto.Models;

namespace BoletosEventto.Controllers
{
    public class CarritoController : ApiController
    {
		/*private BoletosEventtoContext db = new BoletosEventtoContext();

		public IQueryable<Carrito> GetCarritos()
		{
			return db.Carrito.Include(c => c.Boleto).Include(c => c.Usuario);

		}
		[ResponseType(typeof(Carrito))]
		public async Task<IHttpActionResult> GetCarrito(int id)
		{
			Carrito carrito = await db.Carrito.Include(c => c.Usuario).Include(c => c.Boleto).FirstOrDefaultAsync(c => c.CarritoId == id);
			if (carrito == null)
				return NotFound();

			return Ok(carrito);
		}

		[ResponseType(typeof(Carrito))]
		public async Task<IHttpActionResult> PostCarrito(Carrito carrito)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			db.Carrito.Add(carrito);
			await db.SaveChangesAsync();
			return CreatedAtRoute("DefaultApi",
				new { id = carrito.CarritoId }, carrito);

		}

		[ResponseType(typeof(Carrito))]
		public async Task<IHttpActionResult> PutCarrito(int id, Carrito carrito)
		{
			if (!ModelState.IsValid || id != carrito.CarritoId)
			{
				return BadRequest(ModelState);
			}

			db.Entry(carrito).State = EntityState.Modified;
			try
			{
				await db.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				throw;
			}

			return CreatedAtRoute("DefaultApi",
				new { id = carrito.CarritoId }, carrito);

		}

		[ResponseType(typeof(Carrito))]
		public async Task<IHttpActionResult> DeleteCarrito(int id)
		{
			Carrito carrito = await db.Carrito.FindAsync(id);
			if (carrito == null)
			{
				return NotFound();
			}
			db.Carrito.Remove(carrito);
			await db.SaveChangesAsync();

			return CreatedAtRoute("DefaultApi", 
				new { id = carrito.CarritoId }, carrito);
		}


        */

	}
}
