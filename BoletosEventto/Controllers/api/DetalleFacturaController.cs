﻿using BoletosEventto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace BoletosEventto.Controllers
{
    public class DetalleFacturaController : ApiController
    {
		private BoletosEventtoContext db = new BoletosEventtoContext();

		public IQueryable<DetalleFactura> GetDetallesFacturas()
		{
			return db.DetalleFactura.Include(c => c.Boleto).Include(c => c.Factura);

		}

		[ResponseType(typeof(DetalleFactura))]
		public async Task<IHttpActionResult> GetDetalleFactura(int id)
		{
			DetalleFactura contacto = await db.DetalleFactura.Include(c => c.Factura).Include(c => c.Boleto).FirstOrDefaultAsync(c => c.DetalleFacturaId == id);
			if (contacto == null)
				return NotFound();

			return Ok(contacto);
		}
		[ResponseType(typeof(DetalleFactura))]
		public async Task<IHttpActionResult> PostDetalleFactura(DetalleFactura usuario)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			db.DetalleFactura.Add(usuario);
			await db.SaveChangesAsync();
			return CreatedAtRoute("DefaultApi",
				new { id = usuario.DetalleFacturaId }, usuario);

		}
		[ResponseType(typeof(DetalleFactura))]
		public async Task<IHttpActionResult> PutDetalleFactura(int id, DetalleFactura detalle)
		{
			if (!ModelState.IsValid || id != detalle.DetalleFacturaId)
			{
				return BadRequest(ModelState);
			}

			db.Entry(detalle).State = EntityState.Modified;
			try
			{
				await db.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				throw;
			}

			return CreatedAtRoute("DefaultApi",
				new { id = detalle.DetalleFacturaId }, detalle);

		}

		[ResponseType(typeof(DetalleFactura))]
		public async Task<IHttpActionResult> DeleteDetalleFactura(int id)
		{
			DetalleFactura detalle = await db.DetalleFactura.FindAsync(id);
			if (detalle == null)
			{
				return NotFound();
			}
			db.DetalleFactura.Remove(detalle);
			await db.SaveChangesAsync();

			return CreatedAtRoute("DefaultApi", new { id = detalle.DetalleFacturaId }, detalle);
		}
        [HttpGet]
        [Route("api/DetalleFactura/topTicket")]
        [ResponseType(typeof(Boleto))]
        public IHttpActionResult GetTopTicket()
        {
            int cantidad = 0;
            Boleto topB = new Boleto();
            List<Boleto> eventos = db.Boleto.Include(c => c.Evento).ToList();
            foreach (Boleto ev in eventos)
            {
                int cantidad1 = db.DetalleFactura.Where(c => c.BoletoId == ev.BoletoId).Count();
                if (cantidad < cantidad1)
                {
                    cantidad = cantidad1;
                    topB = ev;
                    topB.Cantidad = cantidad;
                }
            }

            return Ok(topB);
        }
        [HttpGet]
        [Route("api/DetalleFactura/Grafica")]
        public IQueryable<Boleto> GetGrafica()
        {
            IQueryable<Boleto>  eventos = db.Boleto.Include(c => c.Evento);
            foreach (Boleto ev in eventos)
            {
                int cantidad1 = db.DetalleFactura.Where(c => c.BoletoId == ev.BoletoId).Count();
                ev.Cantidad = cantidad1;
            }

            return eventos;

        }

    }
}
