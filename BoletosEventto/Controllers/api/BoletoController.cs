﻿using BoletosEventto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace BoletosEventto.Controllers
{
    public class BoletoController : ApiController
    {
		private BoletosEventtoContext db = new BoletosEventtoContext();

		public IQueryable<Boleto> GetBoletos()
		{
			return db.Boleto.Include(c => c.TipoBoleto).Include(c => c.Promocion).Include(c => c.Evento);
		}

		[ResponseType(typeof(Boleto))]
		public async Task<IHttpActionResult> GetBoleto(int id)
		{
			Boleto boleto = await db.Boleto.Include(c => c.Evento).Include(c => c.Promocion).
				Include(c => c.TipoBoleto).FirstOrDefaultAsync(c => c.BoletoId == id);
			if (boleto == null)
				return NotFound();

			return Ok(boleto);
		}

		[ResponseType(typeof(Boleto))]
		public async Task<IHttpActionResult> PostBoleto(Boleto boleto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			db.Boleto.Add(boleto);
			await db.SaveChangesAsync();
			return CreatedAtRoute("DefaultApi",
				new { id = boleto.BoletoId }, boleto);

		}

		[ResponseType(typeof(Boleto))]
		public async Task<IHttpActionResult> PutBoleto(int id, Boleto boleto)
		{
			if (!ModelState.IsValid || id != boleto.BoletoId)
			{
				return BadRequest(ModelState);
			}

			db.Entry(boleto).State = EntityState.Modified;
			try
			{
				await db.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				throw;
			}

			return CreatedAtRoute("DefaultApi",
				new { id = boleto.BoletoId }, boleto);

		}
		[ResponseType(typeof(Boleto))]
		public async Task<IHttpActionResult> DeleteBoleto(int id)
		{
			Boleto boleto = await db.Boleto.FindAsync(id);
			if (boleto == null)
			{
				return NotFound();
			}
			db.Boleto.Remove(boleto);
			await db.SaveChangesAsync();

			return CreatedAtRoute("DefaultApi", new { id = boleto.BoletoId }, boleto);
		}




	}
}
