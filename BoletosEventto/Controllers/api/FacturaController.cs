﻿using BoletosEventto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace BoletosEventto.Controllers
{
    public class FacturaController : ApiController
    {
		private BoletosEventtoContext db = new BoletosEventtoContext();

		public IQueryable<Factura> GetFacturas()
		{
			return db.Factura.OrderBy(c => c.Fecha);

		}
        [HttpGet]
        [Route("api/Factura/{mes}/{ano}")]
        public IQueryable<Factura> GetPorFecha (int mes,int ano)
        {
            return (db.Factura.Where(c => c.Fecha.Month == mes && c.Fecha.Year == ano));
        }

		[ResponseType(typeof(Factura))]
		public async Task<IHttpActionResult> GetFactura(int id)
		{
			Factura factura = await db.Factura.FirstOrDefaultAsync(c => c.FacturaId == id);
			if (factura == null)
				return NotFound();

			return Ok(factura);
		}

		[ResponseType(typeof(Factura))]
		public async Task<IHttpActionResult> PostFactura(Factura factura)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			db.Factura.Add(factura);
			await db.SaveChangesAsync();
			return CreatedAtRoute("DefaultApi",
				new { id = factura.FacturaId }, factura);

		}

		[ResponseType(typeof(Factura))]
		public async Task<IHttpActionResult> PutFactura(int id, Factura factura)
		{
			if (!ModelState.IsValid || id != factura.FacturaId)
			{
				return BadRequest(ModelState);
			}

			db.Entry(factura).State = EntityState.Modified;
			try
			{
				await db.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				throw;
			}

			return CreatedAtRoute("DefaultApi",
				new { id = factura.FacturaId }, factura);

		}

		[ResponseType(typeof(Usuario))]
		public async Task<IHttpActionResult> DeleteFactura(int id)
		{
			Factura factura = await db.Factura.FindAsync(id);
			if (factura == null)
			{
				return NotFound();
			}
			db.Factura.Remove(factura);
			await db.SaveChangesAsync();

			return CreatedAtRoute("DefaultApi", new { id = factura.FacturaId }, factura);
		}
	}
}
