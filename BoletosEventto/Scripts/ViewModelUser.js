﻿var ViewModel = function () {
    var main = this;
    var userUri = '../api/Usuario/';
    main.error = ko.observable();
    main.users = ko.observableArray();
    main.roles = ko.observableArray();
    main.nuevo = {
        Nick: ko.observable(),
        Contrasena: ko.observable(),
        Rol: ko.observable()
    }
    main.agregar = function (formElement) {
        var tipo = {
            Nick: main.nuevo.Nick(),
            Contrasena: main.nuevo.Contrasena(),
            RolId: main.nuevo.Rol().RolId
        }
        ajaxHelper(userUri, 'POST', tipo)
            .done(function (data) {
                getAllUser();
            });
    }
    main.eliminar = function (item) {
        var id = item.UsuarioId;
        var uri = userUri + id;
        ajaxHelper(uri, 'DELETE').done(function () {
            getAllUser();
        });
    }

    function ajaxHelper(uri, method, data) {
        main.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            main.error(errorThrown);
        });
    }
    function getAllUser() {
        ajaxHelper(userUri, 'GET')
            .done(function (data) {
                main.users(data);
            });
    }
    function getAllRol() {
        ajaxHelper('../api/Rol/', 'GET').done(function (data) {
            main.roles(data);
        });
    }
    getAllUser();
    getAllRol();
}
$(document).ready(function () {
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);
});