﻿var ViewModel = function () {
    var main = this;
    var eventoUri = '../api/Evento/';
    main.error = ko.observable();
    main.eventos = ko.observableArray();

    function ajaxHelper(uri, method, data) {
        main.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            main.error(errorThrown);
        });
    }
    function getAllEventos() {
        ajaxHelper(eventoUri, 'GET')
            .done(function (data) {
                main.eventos(data);
            });
    }
    getAllEventos();
}
$(document).ready(function () {
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);
});