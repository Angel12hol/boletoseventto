﻿var ViewModel = function () {
    var main = this;
    var userUri = 'api/Usuario/'
    main.nick = ko.observable();
    main.error = ko.observable();
    main.bLogin = ko.observable();
    main.bRegistrar = ko.observable();
    main.contrasena = ko.observable();
    ko.options.useOnlyNativeEvents = true;
    main.ingresar = function (formElement) {
        var uri = userUri + "login/" + main.nick() + "/" + main.contrasena();
        ajaxHelper(uri, 'GET').done(function (data) {
            if (data.result === "false") {
                main.error(data.result);
            }else {
                console.log("U no pos si ")
                '<%Session["UserName"] = "' + data.nick + '"; %>';
                alert('<%=Session["UserName"] %>');
            }
        });
    }
    main.eIngresa = function () {
        main.bLogin(true);
        main.bRegistrar('');
        console.log("Hola")
    }
    main.eRegistra = function () {
        main.bLogin('');
        main.bRegistrar(true);
    }
    function ajaxHelper(uri, method, data) {
        main.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            main.error(errorThrown);
        });
    }
}
$(document).ready(function () {
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);
});