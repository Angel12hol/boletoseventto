﻿var ViewModel = function() {
    var main = this;
    var tipoUri = '../api/TipoBoleto/';
    main.error = ko.observable();
    main.tipos = ko.observableArray();
    main.tipoCargado = ko.observable();
    main.tipoNuevo = {
        Descripcion: ko.observable()
    }
    main.cargar = function (item) {
        main.tipoCargado(item);
    }
    main.editar = function (formElement) {
        var tipoEditado = {
            TipoBoletoId: main.tipoCargado().TipoBoletoId,
            Descripcion: main.tipoCargado().Descripcion
        }
        var uri = tipoUri + tipoEditado.TipoBoletoId;
        ajaxHelper(uri, 'PUT', tipoEditado)
            .done(function (data) {
                getAllTiposBoletos();
                main.tipoCargado('');
            })
    }
    main.agregar = function (formElement) {
        var tipo = {
            Descripcion: main.tipoNuevo.Descripcion()
        }
        ajaxHelper(tipoUri, 'POST', tipo)
            .done(function (data) {
                getAllTiposBoletos();
            });
    }
    main.eliminar = function (item) {
        var id = item.TipoBoletoId;
        var uri = tipoUri + id;
        ajaxHelper(uri, 'DELETE').done(function () {
            getAllTiposBoletos();
        });
    }
    main.cancelar = function () {
        main.tipoCargado('');
    }

    function ajaxHelper(uri, method, data) {
        main.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            main.error(errorThrown);
        });
    }
    function getAllTiposBoletos() {
        ajaxHelper(tipoUri, 'GET')
            .done(function (data) {
                main.tipos(data);
            });
    }
    getAllTiposBoletos();
}
$(document).ready(function () {
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);
});