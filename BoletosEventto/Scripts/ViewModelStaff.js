﻿var ViewModel = function () {
    var main = this;
    var staffUri = '../api/Staff/';
    main.error = ko.observable();
    main.staffs = ko.observableArray();
    main.cargado = ko.observable();
    main.nuevo = {
        Trabajador: ko.observable(),
        Costo: ko.observable()
    }
    main.cargar = function (item) {
        main.cargado(item);
    }
    main.editar = function (formElement) {
        var editado = {
            StaffId: main.cargado().StaffId,
            Trabajador: main.cargado().Trabajador,
            Costo: main.cargado().Costo
        }
        var uri = staffUri + editado.StaffId;
        ajaxHelper(uri, 'PUT', editado)
            .done(function (data) {
                getAllStaff();
                main.cargado('');
            })
    }
    main.agregar = function (formElement) {
        var tipo = {
            Trabajador: main.nuevo.Trabajador(),
            Costo: main.nuevo.Costo()
        }
        ajaxHelper(staffUri, 'POST', tipo)
            .done(function (data) {
                getAllStaff();
            });
    }
    main.eliminar = function (item) {
        var id = item.StaffId;
        var uri = staffUri + id;
        ajaxHelper(uri, 'DELETE').done(function () {
            getAllStaff();
        });
    }
    main.cancelar = function () {
        main.cargado('');
    }

    function ajaxHelper(uri, method, data) {
        main.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            main.error(errorThrown);
        });
    }
    function getAllStaff() {
        ajaxHelper(staffUri, 'GET')
            .done(function (data) {
                main.staffs(data);
            });
    }
    getAllStaff();
}
$(document).ready(function () {
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);
});