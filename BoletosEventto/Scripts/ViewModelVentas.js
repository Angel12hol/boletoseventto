﻿var ViewModel = function () {
    var main = this;
    var facturaUri = '../api/Factura/';
    main.error = ko.observable();
    main.facturas = ko.observableArray();
    main.TopUsuario = ko.observable();
    main.topEvento = ko.observable();
    main.topBoleto = ko.observable();
    main.top = {
        topEvento: ko.observable(),
        topBoleto: ko.observable()
    }
    main.grafo = ko.observable();
    main.generarGraficaFac = function (formElement) {
        var ctx = document.getElementById("myChart");
        var labels = [];
        var aregloArr = [];
        

        $.each(areglo2, function (key, value) {
            aregloArr.push(value.Cantidad);
            labels.push(value.Evento.Descripcion);
        });

        console.log(JSON.stringify(aregloArr))
        console.log(JSON.stringify(labels));


        var data = {
            labels: labels,
            datasets: [
                {
                    label: "Tickets Vendidos",
                    backgroundColor: [
                        'rgba(255, 99, 132,0 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1,
                    data: aregloArr,
                }
            ]
        };        
        new Chart(ctx, {
            type: "bar",
            data: data,
            options: {
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                },
                responsive: false
            }
        });        

    }
    
    var areglo2 = []; 

    function llenarGrafica() {
        ajaxHelper('../api/DetalleFactura/Grafica', 'GET')
            .done(function (data) {
                console.log(JSON.stringify(data))
                areglo2 = data;
            });

    }
    function ajaxHelper(uri, method, data) {
        main.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            main.error(errorThrown);
        });
    }
    function getAllFacturas() {
        ajaxHelper(facturaUri, 'GET')
            .done(function (data) {
                main.facturas(data);
            });
    }
    main.cantidad = ko.observable();
    function getTopBoleto() {
        ajaxHelper('../api/DetalleFactura/topTicket', 'GET').done(function (data) {
            main.top.topBoleto(data.Cantidad);
            main.top.topEvento(data.Evento.Descripcion);
        });
    }


    llenarGrafica();
    getAllFacturas();
    getTopBoleto();
}
$(document).ready(function () {
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);
});