﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BoletosEventto.Models
{
    public class DetalleStaff
    {
        public int DetalleStaffId { get; set; }
        [Required]
        public int StaffId { get; set; }
        [Required]
        public int EventoId { get; set; }
        [Required]
        public int Cantidad { get; set; }
        public Staff Staff { get; set; }
        public Evento Evento { get; set; }
    }
}