﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BoletosEventto.Models
{
    public class DetalleFactura
    {
        public int DetalleFacturaId { get; set; }
        [Required]
        public int BoletoId { get; set; }
        [Required]
        public int FacturaId { get; set; }
        [Required]
        public int Asiento { get; set; }
        public Boleto Boleto { get; set; }
        public Factura Factura { get; set; }
    }
}