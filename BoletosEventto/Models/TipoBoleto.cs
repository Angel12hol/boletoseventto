﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BoletosEventto.Models
{
    public class TipoBoleto
    {
        public int TipoBoletoId { get; set; }
        [Required, StringLength(15)]
        public String Descripcion { get; set; }
    }
}