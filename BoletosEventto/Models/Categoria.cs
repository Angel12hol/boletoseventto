﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BoletosEventto.Models
{
    public class Categoria
    {
        public int CategoriaId { get; set; }
        [Required, StringLength(20)]
        public String Descripcion { get; set; }
    }
}