﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BoletosEventto.Models
{
    public class Usuario
    {
        public int UsuarioId { get; set; }
        [Required, StringLength(50)]
        public String Nick { get; set; }
        [Required, StringLength(50)]
        public String Contrasena { get; set; }
        [Required]
        public int RolId { get; set; }
        public Rol Rol { get; set; }
    }
}