﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BoletosEventto.Models
{
    public class Boleto
    {
        public int BoletoId { get; set; }
        [Required]
        public double Precio { get; set; }
        [Required]
        public int Cantidad { get; set; }
        [Required]
        public int EventoId { get; set; }
        [Required]
        public int TipoBoletoId { get; set; }
        [Required]
        public int PromocionId { get; set; }
        public Evento Evento { get; set; }
        public TipoBoleto TipoBoleto { get; set; }
        public Promocion Promocion { get; set; }
    }
}