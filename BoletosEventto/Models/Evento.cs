﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BoletosEventto.Models
{
    public class Evento
    {
        public int EventoId { get; set; }
        [Required, StringLength(50)]
        public String Descripcion { get; set; }
        [Required, StringLength(20)]
        public String Fecha { get; set; }
        [Required,StringLength(30)]
        public String Localidad { get; set; }
        [Required]
        public int CategoriaId { get; set; }
        [Required]
        public int Entradas { get; set; }
        [Required]
        public int UsuarioId { get; set; }
        public int Columna { get; set; }
        public Categoria Categoria { get; set; }
        public Usuario Usuario { get; set; }
    }
}