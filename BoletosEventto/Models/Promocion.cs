﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BoletosEventto.Models
{
    public class Promocion
    {
        public int PromocionId { get; set; }
        [Required, StringLength(30)]
        public String Descripcion { get; set; }
        [Required]
        public Double Descuento { get; set; }
    }
}