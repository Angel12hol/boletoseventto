﻿namespace BoletosEventto.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class BoletosEventtoContext : DbContext
    {
        // El contexto se ha configurado para usar una cadena de conexión 'BoletosEventtoContext' del archivo 
        // de configuración de la aplicación (App.config o Web.config). De forma predeterminada, 
        // esta cadena de conexión tiene como destino la base de datos 'BoletosEventto.Models.BoletosEventtoContext' de la instancia LocalDb. 
        // 
        // Si desea tener como destino una base de datos y/o un proveedor de base de datos diferente, 
        // modifique la cadena de conexión 'BoletosEventtoContext'  en el archivo de configuración de la aplicación.
        public BoletosEventtoContext()
            : base("name=BoletosEventtoContext")
        {
        }
        
        public DbSet<Rol> Rol { get; set; }
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Categoria> Categoria { get; set; }
        public DbSet<Promocion> Promocion { get; set; }
        public DbSet<TipoBoleto> TipoBoleto { get; set; }
        public DbSet<Evento> Evento { get; set; }
        public DbSet<Boleto> Boleto { get; set; }
        public DbSet<Staff> Staff { get; set; }
        public DbSet<DetalleStaff> DetalleStaff { get; set; }
        public DbSet<Factura> Factura { get; set; }
        public DbSet<DetalleFactura> DetalleFactura { get; set; }
    }
}