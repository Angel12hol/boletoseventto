﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BoletosEventto.Models
{
    public class Carrito
    {
        public int CarritoId { get; set; }
        [Required]
        public int UsuarioId { get; set; }
        [Required]
        public int BoletoId { get; set; }
        [Required]
        public int Cantidad { get; set; }
        public Usuario Usuario { get; set; }
        public Boleto Boleto { get; set; }
    }
}