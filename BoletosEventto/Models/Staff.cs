﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BoletosEventto.Models
{
    public class Staff
    {
        public int StaffId { get; set; }
        [Required]
        public String Trabajador { get; set; }
        [Required]
        public Double Costo { get; set; }
    }
}