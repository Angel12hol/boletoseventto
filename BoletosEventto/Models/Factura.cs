﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BoletosEventto.Models
{
    public class Factura
    {
        public int FacturaId { get; set; }
        [Required, StringLength(30)]
        public String Cliente { get; set; }
        [Required]
        public DateTime Fecha { get; set; }
        [Required]
        public Double Total { get; set; }
        [Required,StringLength(25)]
        public String Transaccion { get; set; }
    }
}